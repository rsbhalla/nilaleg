//Note: the output isn't scaled
var collection = ee.ImageCollection("MODIS/006/MOD17A3H")
    .filterDate('2000-01-01', '2018-10-01')
    .filterBounds(bbox);
print('collection', collection);
print('Number of images in collection:', collection.size());
// Compute the mean in each band, in each pixel.
//var maxnnpp = collection.max();
//var mediannpp = collection.median();
var meannpp = collection.mean();

var nppint = meannpp.uint16()
print('Mean NPP', meannpp);

// Display the result.
Map.addLayer(nppint.select(0).clip(bbox), {min:-3000, max:32700}, 'nppint');

// Create a task that you can launch from the Tasks tab.
Export.image.toDrive({
  image: nppint,
  description: 'MODIS_meannpp_2000_2014',
  region: bbox,
  maxPixels: 10000000000,
  folder: 'EE',
  scale: 500
});