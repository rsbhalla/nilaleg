// source: https://developers.google.com/earth-engine/ic_reducing
// This function adds a band representing the image timestamp.
var addTime = function(image) {
  return image.addBands(image.metadata('system:time_start')
    .divide(1000 * 60 * 60 * 24 * 365));
};

var scaleNPP = function(image) {
  var nppScaled = image.expression('float(b("Npp")/1000)').rename('NPP_MgCha');
  // note: scaling is done from MODIS to NPP values (divided by 10000) and from the kg C/m (MODIS) to Mg C/ha (UNDP GEF).
  // calculations kg to megagram (Mg) is divided by 1000. metres to hectares is multiplied by 10000
  return image.addBands(nppScaled);
};


// Load a MODIS collection, filter to several years of 16 day mosaics,
// and map the time band function over it.
var collection = ee.ImageCollection("MODIS/006/MOD17A3H")
  .filterDate('2011-01-01', '2013-01-31')
  .filterBounds(bbox)
  .map(addTime)
  .aside(print) //uncomment this aside to print intermediate result
  .map(scaleNPP);
print(collection);

// Select the bands to model with the independent variable first.
var trend = collection.select(['system:time_start', 'NPP_MgCha'])
  // Compute the linear trend over time.
  .reduce(ee.Reducer.linearFit());

// Display the trend with increasing slopes in green, decreasing in red.
Map.addLayer(
    trend .clip(bbox),
    {min: 0, max: [-100, 100, 10000], bands: ['scale', 'scale', 'offset']},'NPP trend');

// Create a task that you can launch from the Tasks tab.
Export.image.toDrive({
  image: trend,
  folder: 'EE',
  description: 'bbox_MODIS_TERRA_NPP_TREND_MgCha_Jan11ToJan13',
  region: bbox,
  maxPixels: 1000000000,
  scale: 500
});
