// Modified from: https://developers.google.com/earth-engine/ic_reducing
// This function adds a band representing the image timestamp.
// Need to create an asset NNam - shapefile for northern Namibia
var addTime = function(image) {
  return image.addBands(image.metadata('system:time_start')
    .divide(1000 * 60 * 60 * 24 * 365));
};
// scale the image to actual values
var scaleNDVI = function(image) {
  var ndviScaled = image.expression('float(b("NDVI")/10000)').rename('ndviScaled');
  return image.addBands(ndviScaled);
};

// Load a MODIS collection, filter to several years of 16 day mosaics,
// clip to image extent
// and map the time band function over it.
var collection = ee.ImageCollection('MODIS/006/MYD13A1')
 .filterDate('2003-01-01', '2018-07-01')
  .filterBounds(NNam)
  .map(addTime)
  // .aside(print) //uncomment this aside to print intermediate result
  .map(scaleNDVI);
print(collection);

// Select the bands to model with the independent variable first.
var trend = collection.select(['system:time_start', 'ndviScaled']) //replaced NDVI with ndviScaled
  // Compute the linear trend over time.
  .reduce(ee.Reducer.linearFit());

// Display the trend with increasing slopes in green, decreasing in red.
Map.addLayer(
    trend .clip(NNam),
    {min: 0, max: [-100, 100, 10000], bands: ['scale', 'scale', 'offset']},
    'NDVI trend');

// Create a task that you can launch from the Tasks tab.
Export.image.toDrive({
  image: trend,
  folder: 'EE',
  description: 'NNam_MODIS_TERRA_NDVI_TREND',
  region: NNam,
  maxPixels: 1000000000,
  scale: 500
});
