// upload the asset bbox - shapefile containing area of interest
var collection = ee.ImageCollection('LANDSAT/LC08/C01/T1_ANNUAL_NDVI')
    .filterDate('2003-01-01', '2018-12-01')
    .filterBounds(bbox);
print('collection', collection);
print('Number of images in collection:', collection.size());
var mean = collection.mean();
print('Mean NDVI 01 Jan 2013 to 01 Dec 18', mean);
// Display the result.
Map.addLayer(mean.select(0).clip(bbox), {min:0, max:0.8}, 'mean');
// Create a task that you can launch from the Tasks tab.
Export.image.toDrive({
  image: mean,
  description: 'MeanNDVI_Landsat8_TOA_C1_5years',
  region: bbox,
  folder: 'EE',
  maxPixels: 28000000000,
  scale: 30
});