//function to scale the MODIS to value in Mg C per hectares per year.
// note that the scaling is 10000 but we multiply by 10 to change from kg C/m to Mg C/ha
var scaleNPP = function(image) {
  var nppScaled = image.expression('float(b("Npp")/1000)').rename('Npp_scaled');
  return image.addBands(nppScaled);
};
//filter and scale collection
var collection = ee.ImageCollection("MODIS/006/MOD17A3H")
    .filterDate('2000-01-01', '2014-01-31')
    .filterBounds(bbox)
    .map(scaleNPP);

// display values
print('collection', collection);
print('Number of images in collection:', collection.size());

// Compute the desired statistic in each band, in each pixel.
var maxnpp = collection.max();
var maxnppflt = maxnpp.select(2).float()
var meannpp = collection.mean();
var meannppflt = meannpp.select(2).float()
var minnpp = collection.min();
var minnppflt = minnpp.select(2).float()

// merge images to collection
var NPPstats = ee.ImageCollection.fromImages(
  [ee.Image(maxnppflt), ee.Image(meannppflt), ee.Image(minnppflt)]);
print('NPPstats: ', NPPstats);

// Stack the image collection so it can be exported as a multiband image
var stackCollection = function(collection) {
  // Create an initial image.
  var first = ee.Image(collection.first()).select([]);
  // Write a function that appends a band to an image.
  var appendBands = function(image, previous) {
    return ee.Image(previous).addBands(image);
  };
  return ee.Image(collection.iterate(appendBands, first));
};
var stacked = stackCollection(NPPstats);
// rename
var bands = ['Max', 'Mean', 'Min'];
var stacked = stacked.rename(bands);

print('stacked image', stacked);

// Display the first band of the stacked image.
Map.addLayer(
    stacked .clip(bbox),
    {min:0, max:15}, 'stacked');

Export.image.toDrive({
  image: stacked,
  description: 'MODIS_NPP_MgCha_statsJan00toJan14',
  region: bbox,
  maxPixels: 10000000000,
  folder: 'EE',
  scale: 500
});