// function to scale data to MgC/ha from KgC/m and adjust for MODIS scale (factor of 10000)
var scaleNPP = function(image) {
  var nppScaled = image.expression('float(b("Npp")/1000)').rename('NPP_MgCha');
  return image.addBands(nppScaled);
};
//filter and scale collection
var collection = ee.ImageCollection("MODIS/006/MOD17A3H")
    .filterDate('2000-01-01', '2018-10-01')
    .filterBounds(bbox)
    .map(scaleNPP);
print('collection', collection);
print('Number of images in collection:', collection.size());
// Compute the mean in each band, in each pixel.
//var maxnnpp = collection.max();
//var mediannpp = collection.median();
var meannpp = collection.mean();

// var nppint = meannpp.uint16()
// convert to float
var nppflt = meannpp.select(2).float()
print('Mean NPP', nppflt);

// Display the result.
//Map.addLayer(nppint.select(0).clip(bbox), {min:-3000, max:32700}, 'nppint');
Map.addLayer(nppflt.select(0).clip(bbox), {min:-3000, max:32700}, 'nppflt');

// Create a task that you can launch from the Tasks tab.
Export.image.toDrive({
  image: nppflt,
  description: 'MODIS_meannpp_MgCha_2000_2014',
  region: bbox,
  maxPixels: 10000000000,
  folder: 'EE',
  scale: 500
});