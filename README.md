# NILALEG

Code and resources for the NILALEG project. This work is being shared on an as is basis and comes with no warranties whatsoever. This work was supported by the Energy and Environment units of the UNDP - Namibia. Please contact the UNDP Namibia for details of copyright and permission for re-use.

Energy & Environment for Sustainable Development 
United Nations Development Programme 
UN House, 38 Stein St. Private Bag 13329 
Klein Windhoek, Windhoek, Namibia